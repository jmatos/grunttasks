'use strict';

/*
    Below is an explanation of the parameters/options used in this task.
        For more information on options see: http://gruntjs.com/api/grunt.option

    --WEBDAV_SERVER
        The server to download the log files from.
    --WEBDAV_USERNAME
        Username for the WebDav server
    --WEBDAV_PASSWORD
        Password for the WebDav server
    --MAX_CODE_VERSIONS
        The number of code versions to keep on the server.  Oldest versions are deleted first.

    Modify the 'default' variables velow if you intend to run this grunt task without
    parameters.
    */


var default_webdav_server = '';
var default_webdav_username = '';
var default_webdav_password = '';
var default_max_code_versions = '3';

module.exports = function(grunt) {
    require('time-grunt')(grunt);
     
    grunt.initConfig({              
        codeversioncleaner: {
            options: {              
                webdav_server: grunt.option('WEBDAV_SERVER') || default_webdav_server,
                webdav_username: grunt.option('WEBDAV_USERNAME') || default_webdav_username,
                webdav_password: grunt.option('WEBDAV_PASSWORD') || default_webdav_password,
                max_code_versions: grunt.option('MAX_CODE_VERSIONS') || default_max_code_versions
            }
        }
    }); 
        
    grunt.loadTasks("./tasks");
    grunt.registerTask('default', ['codeversioncleaner']);
};
